# Parrot Word Generator

![Parrot Word Generator](pwg.gif)


PWG generates big words using emotes for your favorite messaging client.
Simply type in the names of any two emotes, and a phrase to translate, and PWG does the rest.

[Click here][homepage] to see it in action!


[homepage]: https://parrot.kyle.technology/
